

<!-- Readme.md is generated from Readme.org. Please edit that file -->


# ggmissing R package

This package provides visualizations of missingness in data.frames.

## Example Usage

We use the mtcars data:

```R
dat <- mtcars
```

For illustration purposes, we introduce some missing values:

```R
for (i in 1:15) dat[sample(nrow(dat), 1), sample(ncol(dat), 1)] <- NA
```

Unsorted visualization:

```R
ggmissing(dat)
```

![img](README-mtcars-unsorted.png)

Sorted visualization:

```R
ggmissing(dat, sort = TRUE)
```

![img](README-mtcars-sorted.png)

## Installation

Installation directly from gitlab is possible, when the package
[devtools](https://cran.r-project.org/package=devtools) is installed.

Once the dependencies are installed, get this package with

```R
library("devtools")
install_git("https://gitlab.gwdg.de/aleha/ggmissing")
```

## Roadmap

-   move legend to top right panel
-   fix `R CMD check` NOTEs about undefined variables from NSE


<!--
Local Variables:
 mode: gfm
 markdown-command: "marked"
End:
-->
